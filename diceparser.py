# Imports. Important.
import random
import utils as u

tracing=True
ops=['+','-','*','/',':','k','t','r','!','d']
func=['add','','','','','','','','','roll']

# Funcdef
def roll(input):
 if len(input)!=2:
  return input
 i=0
 sum=0
 u.trace(input)
 while(i<int(input[0])):
  result=int(random.random()*int(input[1]))+1
  i+=1
  u.trace(result)
  sum+=result
 return result

#def explode(input):
# pass

def add(input):
 return sum(input)

# Arrange brackets:
def deparen(input):
 # Do it serialised, because it'll be simpler.
 i=0
 down=[]
 store=[]
 while i<len(input):
  if input[i]=='(':
   down.append(i)
  elif input[i]==')':
   start=down.pop()
   stash=input[start:i+1]
   store.append(stash[1:-1])
   input=input.replace(stash,"i"+str(len(store)),1)
   i=start
  i+=1
 store.append(input)
 u.trace(store)
 # Array built. Next trick, parse each one in turn then return.
 i=0
 while len(store)>1:
  store[0]=recurvesplit(store[0],0)
  temp=store.pop(0)
  for j in store:
   if "i"+i in j:
    j=j.replace("i"+i,temp,1)
    break
  i+=1
 store=recurvesplit(store[0],0)
 u.trace(store)
 return store

# Primary driver
def recurvesplit(input, n):
 u.trace(input)
 u.trace(type(input))
 if n<len(ops):
  delim=ops[n]
  store=input.split(delim)
  u.trace(func[n])
  u.trace(ops[n])
  u.trace(store)
  for i in range(len(store)):
   store[i]=recurvesplit(store[i],n+1)
  if func[n]!='':
   return globals()[func[n]](store)
  return ops[n].join(store)
 return input

# Spaghetti code is go
#toparse=input("Enter dice string: ")
#dicearray=[int(n) for n in toparse.split('d')]
#roll(dicearray[0],dicearray[1])
#recurvesplit(toparse, 0)
#deparen(toparse)
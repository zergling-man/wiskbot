import asyncio
import datetime as dt
import string as st
import utils as u
#Stupid imports
import discord
import diceparser as dp
import random as ra
import requests as r

client=None

async def quit(args, channel, message):
 u.trace('Goodbye')
 await client.logout()
 exit()

async def evaluate(args, channel, message):
 if args[0]=='await':
  await eval(' '.join(args[1:]), globals(), locals())
 else:
  eval(' '.join(args), globals(), locals())

async def execute(args, channel, message):
 if args[0]=='await':
  await exec(' '.join(args[1:]), globals(), locals())
 else:
  exec(' '.join(args), globals(), locals())

async def prunetest(args, channel, message):
 serv=channel.guild
 chans=serv.channels
 mems={mem.id:mem for mem in serv.members if not mem.bot}
 found={}
 week=dt.datetime.today()-dt.timedelta(days=7)
 u.trace(week)
 for chan in chans:
  try:
   async for log in chan.history(limit=999,after=week):
    id=log.author.id
    if id in mems and (id not in found or found[id] < log.created_at):
     found[id]=log.created_at
  except:
   pass
 for a in found: mems.pop(a,None)
 queue={}
 buffer=maxlon=1998
 for id,mem in mems.items():
  l=len(mem.name)+4
  if l<buffer:
   queue[id]=mem
   buffer-=l
  else:
   await channel.send(list(mem.name for id,mem in queue.items()))
   buffer=maxlon
   queue={}
 if queue!={}:
  await channel.send(list(mem.name for id,mem in queue.items()))
 u.trace("Done prune-testing")

async def prune(args, channel, message):
 serv=channel.guild
 chans=serv.channels
 mems={mem.id:mem for mem in serv.members if not mem.bot}
 found={}
 week=dt.datetime.today()-dt.timedelta(days=7)
 u.trace(week)
 for chan in chans:
  try:
   async for log in chan.history(limit=999,after=week):
    id=log.author.id
    if id in mems and (id not in found or found[id] < log.created_at):
     found[id]=log.created_at
  except:
   pass
 for a in found: mems.pop(a,None)
 for id,mem in mems.items():
  if len(mem.roles)>1:
   try:
    await mem.edit(roles=[],reason='Purged')
    await asyncio.sleep(0.5)
    await channel.send(("%s got purged." % mem.mention))
    await asyncio.sleep(1)
   except:
    pass
 u.trace("Done pruning")

async def count(args, channel, message):
 await message.edit(content=message.content+" %d" % channel.guild.member_count)

async def userinfo(args, channel, message):
 u.trace("User info. TODO.")
 member=channel.guild.get_member(int(args[0]))
 await channel.send(member)

async def sub(args, channel, message):
 me=channel.guild.me
 msg=''
 async for log in channel.history(limit=20):
  if log.channel==channel and log.author==me and log.id!=message.id and msg=='':
   msg=log
   u.trace("found")
 if msg!='':
  newmsg=msg.content
  longth=len(args[0])
  pos=newmsg.find(args[0])
  if pos>-1:
   newmsg=newmsg[:pos]+args[1]+newmsg[pos+longth:]
   await msg.edit(content=newmsg)
   await message.delete()
  else:
   await message.edit(content=message.content+" Not found")

async def eightball(args, channel, message):
 await message.edit(content=("%s %s" % (message.content,ra.choice(u.balls))))

async def help(args, channel, message):
 await message.edit(content=u.commands)

async def macro(args, channel, message):
 with u.curs() as cursor:
  cursor.execute('select response from macros where name = "%s";' % args[0])
  result=cursor.fetchone()
 ph=u.formats(result['response'])
 args[ph]=' '.join(args[ph:])
 await channel.send(result['response'].format(*args[1:]))
 await message.delete()

async def macroadd(args, channel, message):
 with u.curs() as cursor:
  if len(args)==1:
   #lazy remove code
   cursor.execute('delete from macros where name = "%s";' % args[0])
  else:
   cursor.execute('insert into macros (name, response) values ("{0}", "{1}") on duplicate key update response = "{1}";'.format(args[0], " ".join(args[1:]).replace('"','\\"')))
 u.dbcon.commit()

async def macrolist(args, channel, message):
 with u.curs() as cursor:
  cursor.execute('select name from macros')
  m=cursor.fetchall()
 await channel.send(m)

async def save(args, channel, message):
 u.lerg[:]=[]
 async for log in channel.history(limit=20):
  u.lerg.append(log)

async def recall(args, channel, message):
 await channel.send("\n".join(list(u.prettify(message) for message in u.lerg)))

async def spam(args, channel, message):
 chars=list(st.ascii_lowercase)
 chars.append(' ')
 mes=list(u.wrandom(chars, u.lfreq) for i in range(0,1000))
 await channel.send("".join(mes))

async def test(args, channel, message):
 pass

async def send(args, channel, message):
 await u.getchannel(args[1],u.getguild(args[0],client)).send(' '.join(args[2:]))

async def merge(args, channel, message):
 if channel.guild.large:
  await client.request_offline_members(channel.guild)
 fromr=u.getrole(args[0],channel.guild)
 u.trace("From: %s" % fromr.name)
 tor=u.getrole(args[1],channel.guild)
 u.trace("To: %s" % tor.name)
 tomerge = fromr.members
 u.trace("Users: %s" % ", ".join([user.name for user in tomerge]))
 for user in tomerge:
  await user.remove_roles(fromr)
  u.trace("Removing %s from %s" % (fromr.name, user.name))
  await asyncio.sleep(1)
  await user.add_roles(tor)
  u.trace("Adding %s to %s" % (tor.name, user.name))
  await asyncio.sleep(1)

async def roll(args, channel, message):
 await message.edit(content="%s %s" % (message.content,dp.deparen(args[0])))

async def restore(args, channel, message):
 n=int(args[0]) if len(args)>0 else 1
 await channel.send(u.prettify(u.deletesies[len(u.deletesies)-n]))

async def unalter(args, channel, message):
 n=int(args[0]) if len(args)>0 else 1
 await channel.send(u.prettify(u.edits[len(u.edits)-n]))

async def reroll(args, channel, message):
 a=[user.name for user in client.users if int(user.discriminator)==int(args[0])]
 await channel.send(a)

async def prefix(args, channel, message):
 u.dbconfset('prefix',args[0])
 u.p=args[0]

async def status(args, channel, message):
 type=0
 try:
  type=u.gametypes[args[0]]
 except:
  u.trace('Not a valid playing type!')
 u.dbconfset('status',":".join((str(type)," ".join(args[1:]))))
 try:
  await client.change_presence(game=discord.Game(name=" ".join(args[1:]),type=type))
 except:
  u.trace('Failed to set game via command.')

async def deletes(args, channel, message):
 await channel.send("\n".join(list(u.prettify(message) for message in u.deletesies if message is not None)))

#Credit to RiotIsBored#5281(300625132689555457) for reminding me of how fun this is.
async def reverse(args, channel, message):
 await message.edit(content=' '.join(args)[::-1])

async def lipsum(args, channel, message):
 raw=r.get('https://www.lipsum.com/feed/json').json()
 ipsum=raw['feed']['lipsum'][2000::-1]
 space=ipsum.index(' ')
 ipsum=ipsum[:space:-1]
 await message.edit(content=ipsum)

async def geterror(args, channel, message):
 await channel.send(content=u.err)
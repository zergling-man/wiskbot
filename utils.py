import datetime as dt
import discord
import asyncio
import random as ra
import string as st

timestamps='%H:%M:%S'
tracing=True

#Some things that may get moved to the DB:
commands={'evaluate':['eval','e'], 'execute':['exec'], 'sub':[], 'prunetest':['pt'], 'prune':['p'], 'count':['c'], 'userinfo':['u','uinfo'], 'eightball':['8ball','8'], 'help':['h'], 'macro':['m'], 'macroadd':['ma'], 'macrolist':['ml'], 'save':['q'], 'recall':['qr'], 'spam':[], 'test':['t'], 'send':['s'], 'roll':[], 'restore':['r'], 'unalter':[], 'reroll':['rr'],'prefix':[], 'status':['setgame','sg'], 'deletes':['d'], 'reverse':['backwards','b','vandalise'], 'lipsum':['l'], 'geterror':['error','err']}
balls=['No','Yes','Maybe','Ask me again later','If I feel like it','As you wish','Hell naw']
macmess={}
lerg=[]
lfreq=[0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, 0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749, 0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758, 0.00978, 0.02360, 0.00150, 0.01974, 0.00074, 0.18]
deletesies=[None for _ in range(20)]
edits=[None for _ in range(10)]
gametypes={'playing':0,'watching':1,'listening':2}

def trace(message):
 if tracing:
  print("[{0}]: {1}".format(dt.datetime.now().strftime(timestamps), message))

def getguild(name, client):
 return [guild for guild in client.guilds if guild.name==name or guild.id==int(name)][0]

def getrole(name, guild):
 return [role for role in guild.roles if role.name==name or role.id==int(name)][0]

def getchannel(name, guild):
 return [channel for channel in guild.channels if channel.name==name or channel.id==int(name)][0]

def wrandom(items,weightings):
 max=sum(weightings)
 g=ra.random()*max
 for i in range(0,len(items)):
  g-=weightings[i]
  if g<0:
   return items[i]
 #The best exception handling
 return "Somehow it was too fat"

def dbconfget(name):
 with curs() as cursor:
  cursor.execute('select value from config where name="%s"'%name)
  result = cursor.fetchone()
 value = result['value']
 return value

def dbconfset(name,value):
 with curs() as cursor:
  cursor.execute('insert into config (name, value) values ("{0}", "{1}") on duplicate key update value = "{1}";'.format(name,value))
 dbcon.commit()

def prettify(message):
 if message is not None:
  return '[{0}] {1}: {2}'.format(message.created_at.strftime(timestamps), message.author, message.clean_content)

def curs():
 dbcon.ping()
 return dbcon.cursor()

def formats(sss):
 try:
  return max([int(c[1]) for c in st.Formatter().parse(sss) if c[1] is not None])+1
 except ValueError:
  return 0
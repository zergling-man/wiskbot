import discord
import asyncio
import datetime as dt
import random as ra
import re
import string as st
import utils as u
import commands as c
import json
import pymysql

u.p='|'

#Get token:
try:
 a=open("creds.json")
 creds=json.load(a)
 a.close()
except FileNotFoundError as e:
 u.trace(e)
 u.trace("Fill out creds.json.ex and rename it to creds.json. Exiting.")
 exit()

try:
  u.dbcon = pymysql.connect(host=creds['db']['host'],user=creds['db']['user'],password=creds['db']['password'],db='wiskbot',charset='utf8mb4',cursorclass=pymysql.cursors.DictCursor)
except Exception as e:
 u.trace(e)
 u.trace("Something went wrong connecting to the database.")

client=discord.Client()
c.client=client

@client.event
async def on_ready():
 status=u.dbconfget('status')
 if status=='':
  u.trace('No game in DB to set')
 else:
  try:
   await client.change_presence(game=discord.Game(name=status[2:],type=int(status[0])))
  except:
   u.trace('Failed to set game during boot.')
 prefix = u.dbconfget('prefix')
 if prefix=='':
  u.trace('Couldn\'t set the prefix. Defaulting to "|".')
 else:
  u.p=prefix
 u.trace('Blep! %s\n' % client.user.name)

@client.event
async def on_message(message):
 if message.author.id == client.user.id and message.content.startswith(u.p):
  u.trace(message.content)
  inp=message.content[len(u.p):].split(' ')
  com=inp[0]
  args=inp[1:]
  u.trace('Split input into %s and %s' % (com,args))
  #Anti-aliasing. Huehuehue
  for mand, alias in u.commands.items():
   if com in alias:
    u.trace('Dealiased %s to %s' % (com, mand))
    #This is why I name things in shitty ways, for moments like these
    com=mand
  #Like I give a fuck
  try:
   await getattr(c,com)(args, message.channel, message)
   await message.add_reaction('👌') #Don't touch this. Unicode.
  except Exception as ex:
   await message.add_reaction('❌') #Don't touch this. Unicode.
   u.err = ex
   raise

@client.event
async def on_message_delete(message):
 if not message.author.bot:
  u.deletesies=u.deletesies[1:]
  u.deletesies.append(message)

@client.event
async def on_message_edit(before,after):
 if not before.author.bot:
  u.edits=u.edits[1:]
  u.edits.append(before)

client.logout()
#zzz fix this for clean exit
client.run(creds['token'], bot=False)
u.dbcon.close()